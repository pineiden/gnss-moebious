// @generated automatically by Diesel CLI.

diesel::table! {
    dataset (id) {
        id -> Uuid,
        dt_gen -> Timestamptz,
        data -> Jsonb,
        source_id -> Int4,
    }
}

diesel::table! {
    sources (id) {
        id -> Int4,
        name -> Varchar,
        code -> Varchar,
        uri -> Varchar,
        active -> Bool,
        serie -> Int4,
        data_type -> Varchar,
    }
}

diesel::joinable!(dataset -> sources (source_id));

diesel::allow_tables_to_appear_in_same_query!(
    dataset,
    sources,
);
