use chrono;
use chrono::prelude::*;

pub trait HasDtGen {
    fn get_dt_gen(&self) -> DateTime<Utc>;
}
