/*
This module enables the conversion from protocol structs to gprc structs
*/
use gsof_protocol::protocol::gsof::GsofData as GsofProtocol;
use gsof_protocol::protocol::tables::{
    BattMemTable, ECEFTable, GPSTimeTable, HeadTable, SIGMATable,
};

use crate::moebius_protocol::gsof::{
    GsofBattMem, GsofEcef, GsofHead, GsofSigma, GsofTime, OptionalBattMem,
};

use crate::moebius_protocol::Gsof;

#[derive(Debug, Default)]
pub struct MyMoebius {}

use super::time::{dt_utc2ts};
/*
From protocol HeadTable to GsofHead
*/
// #[local_impl]
// impl From<GsofHead> for HeadTable {
//     fn from(proto : GsofHead) -> Self {
//         HeadTable::new((
// 			proto.get_stx(),
// 			proto.get_status(),
// 			proto.get_mtype(),
// 			proto.get_length(),
// 			proto.get_t_num(),
// 			proto.get_page_index(),
// 			proto.get_max_page_index()
// 		))
// }

impl GsofHead {
    pub fn new() -> Self {
        ::std::default::Default::default()
    }

    pub fn get_stx(&self) -> i32 {
        self.stx
    }

    pub fn set_stx(&mut self, val: i32) {
        self.stx = val
    }

    pub fn get_status(&self) -> i32 {
        self.status
    }

    pub fn set_status(&mut self, val: i32) {
        self.status = val
    }

    pub fn get_mtype(&self) -> i32 {
        self.mtype
    }

    pub fn set_mtype(&mut self, val: i32) {
        self.mtype = val
    }

    pub fn get_length(&self) -> i32 {
        self.length
    }

    pub fn set_length(&mut self, val: i32) {
        self.length = val
    }

    pub fn get_t_num(&self) -> i32 {
        self.t_num
    }

    pub fn set_t_num(&mut self, val: i32) {
        self.t_num = val
    }

    pub fn get_page_index(&self) -> i32 {
        self.page_index
    }

    pub fn set_page_index(&mut self, val: i32) {
        self.page_index = val
    }

    pub fn get_max_page_index(&self) -> i32 {
        self.max_page_index
    }

    pub fn set_max_page_index(&mut self, val: i32) {
        self.max_page_index = val
    }
}

impl From<HeadTable> for GsofHead {
    fn from(head: HeadTable) -> Self {
        let mut new_head = GsofHead::new();
        new_head.set_stx(head.stx as i32);
        new_head.set_status(head.status as i32);
        new_head.set_mtype(head.mtype as i32);
        new_head.set_length(head.length as i32);
        new_head.set_t_num(head.t_num as i32);
        new_head.set_page_index(head.page_index as i32);
        new_head.set_max_page_index(head.max_page_index as i32);
        new_head
    }
}

// #[local_impl]
// impl From<GsofEcef> for ECEFTable {
//     fn from(proto : GsofEcef) -> Self {
// 		ECEFTable::new((
// 			proto.get_x_pos(),
// 			proto.get_y_pos(),
// 			proto.get_z_pos(),
// 		))
// 	}
// }

impl GsofEcef {
    pub fn new() -> Self {
        ::std::default::Default::default()
    }

    pub fn get_x_pos(&self) -> f64 {
        self.x_pos
    }

    pub fn set_x_pos(&mut self, val: f64) {
        self.x_pos = val
    }

    pub fn get_y_pos(&self) -> f64 {
        self.y_pos
    }

    pub fn set_y_pos(&mut self, val: f64) {
        self.y_pos = val
    }

    pub fn get_z_pos(&self) -> f64 {
        self.z_pos
    }

    pub fn set_z_pos(&mut self, val: f64) {
        self.z_pos = val
    }
}
impl From<ECEFTable> for GsofEcef {
    fn from(ecef: ECEFTable) -> Self {
        let mut new_ecef = GsofEcef::new();
        new_ecef.set_x_pos(ecef.x_pos);
        new_ecef.set_y_pos(ecef.y_pos);
        new_ecef.set_z_pos(ecef.z_pos);
        new_ecef
    }
}

// #[local_impl]
// impl From<GsofSigma> for SigmaTable {
//     fn from(proto : GsofEcef) -> Self {
// 		SigmaTable::new((
// 			proto.get_position_rms_sig(),
// 			proto.get_sig_east(),
// 			proto.get_sig_nort(),
// 			proto.get_covar(),
// 			proto.get_sig_up(),
// 			proto.get_semi_major(),
// 			proto.get_semi_minor(),
// 			proto.get_orientation(),
// 			proto.get_unit_var_sig(),
// 			proto.get_num_eposh_sig()
// 		))
// 	}
// }

/* GsofTime */
impl From<GPSTimeTable> for GsofTime {
    fn from(time: GPSTimeTable) -> Self {
        let mut new_time = GsofTime::new();
        new_time.set_gps_time(time.gps_time as i32);
        new_time.set_gps_week(time.gps_week as i32);
        new_time.set_svn_num(time.svn_num as i32);
        new_time.set_flags_1(vec![time.flags_1]);
        new_time.set_flags_2(vec![time.flags_2]);
        new_time.set_init_num(time.init_num as i32);
        new_time
    }
}

impl GsofTime {
    pub fn new() -> Self {
        ::std::default::Default::default()
    }

    pub fn get_gps_time(&self) -> i32 {
        self.gps_time
    }

    pub fn set_gps_time(&mut self, val: i32) {
        self.gps_time = val
    }

    pub fn get_gps_week(&self) -> i32 {
        self.gps_week
    }

    pub fn set_gps_week(&mut self, val: i32) {
        self.gps_week = val
    }

    pub fn get_svn_num(&self) -> i32 {
        self.svn_num
    }

    pub fn set_svn_num(&mut self, val: i32) {
        self.svn_num = val
    }

    pub fn get_flags_1(&self) -> Vec<u8> {
        self.flags_1.clone() //into_iter().map(|item| item.clone()).collect()
    }

    pub fn set_flags_1(&mut self, val: Vec<u8>) {
        self.flags_1 = val
    }

    pub fn get_flags_2(&self) -> Vec<u8> {
        self.flags_2.clone() //into_iter().map(|item| item.clone()).collect()
    }

    pub fn set_flags_2(&mut self, val: Vec<u8>) {
        self.flags_2 = val
    }

    pub fn get_init_num(&self) -> i32 {
        self.init_num
    }

    pub fn set_init_num(&mut self, val: i32) {
        self.init_num = val
    }
}

/**/
impl GsofSigma {
    pub fn new() -> Self {
        ::std::default::Default::default()
    }

    pub fn get_position_rms_sig(&self) -> f32 {
        self.position_rms_sig
    }

    pub fn set_position_rms_sig(&mut self, val: f32) {
        self.position_rms_sig = val
    }

    pub fn get_sig_east(&self) -> f32 {
        self.sig_east
    }

    pub fn set_sig_east(&mut self, val: f32) {
        self.sig_east = val
    }

    pub fn get_sig_nort(&self) -> f32 {
        self.sig_nort
    }

    pub fn set_sig_nort(&mut self, val: f32) {
        self.sig_nort = val
    }

    pub fn get_covar_en(&self) -> f32 {
        self.covar_en
    }

    pub fn set_covar_en(&mut self, val: f32) {
        self.covar_en = val
    }

    pub fn get_sig_up(&self) -> f32 {
        self.sig_up
    }

    pub fn set_sig_up(&mut self, val: f32) {
        self.sig_up = val
    }

    pub fn get_semi_major(&self) -> f32 {
        self.semi_major
    }

    pub fn set_semi_major(&mut self, val: f32) {
        self.semi_major = val
    }

    pub fn get_semi_minor(&self) -> f32 {
        self.semi_minor
    }

    pub fn set_semi_minor(&mut self, val: f32) {
        self.semi_minor = val
    }

    pub fn get_orientation(&self) -> f32 {
        self.orientation
    }

    pub fn set_orientation(&mut self, val: f32) {
        self.orientation = val
    }

    pub fn get_unit_var_sig(&self) -> f32 {
        self.unit_var_sig
    }

    pub fn set_unit_var_sig(&mut self, val: f32) {
        self.unit_var_sig = val
    }

    pub fn get_num_eposh_sig(&self) -> i32 {
        self.num_eposh_sig
    }

    pub fn set_num_eposh_sig(&mut self, val: i32) {
        self.num_eposh_sig = val
    }
}

impl From<SIGMATable> for GsofSigma {
    fn from(sigma: SIGMATable) -> Self {
        let mut new_sigma = GsofSigma::new();
        new_sigma.set_position_rms_sig(sigma.position_rms_sig);
        new_sigma.set_sig_east(sigma.sig_east);
        new_sigma.set_sig_nort(sigma.sig_nort);
        new_sigma.set_covar_en(sigma.covar_en);
        new_sigma.set_sig_up(sigma.sig_up);
        new_sigma.set_semi_major(sigma.semi_major);
        new_sigma.set_semi_minor(sigma.semi_minor);
        new_sigma.set_orientation(sigma.orientation);
        new_sigma.set_unit_var_sig(sigma.unit_var_sig);
        new_sigma.set_num_eposh_sig(sigma.num_eposh_sig as i32);
        new_sigma
    }
}

// #[local_impl]
// impl From<GsofBattMem> for BattMemTable {
//     fn from(proto : GsofBattMem) -> Self {
// 		BattMemTable::new((
// 			proto.get_batt_capacity(),
// 			proto.get_remaining_mem()
// 		))
// 	}
// }

impl GsofBattMem {
    pub fn new() -> Self {
        ::std::default::Default::default()
    }

    pub fn get_batt_capacity(&self) -> i32 {
        self.batt_capacity
    }

    pub fn set_batt_capacity(&mut self, val: i32) {
        self.batt_capacity = val
    }

    pub fn get_remaining_mem(&self) -> f64 {
        self.remaining_mem
    }

    pub fn set_remaining_mem(&mut self, val: f64) {
        self.remaining_mem = val
    }
}

impl From<BattMemTable> for GsofBattMem {
    fn from(batt_mem: BattMemTable) -> Self {
        let mut new_batt_mem = GsofBattMem::new();
        new_batt_mem.set_batt_capacity(batt_mem.batt_capacity as i32);
        new_batt_mem.set_remaining_mem(batt_mem.remaining_mem);
        new_batt_mem
    }
}

// #[local_impl]
// impl From<Gsof> for gsof::GsofData {
//     fn from(proto : GsofBattMem) -> Self {
// 		gsof::Gsof {
// 			dt_recv:proto.get_dt_recv(),
// 			dt_gen:proto.get_dt_gen(),
// 			checksum:proto.get_checksum(),
// 			head: proto.get_head().into(),
// 			time: proto.get_time().into(),
// 			ecef: proto.get_ecef().into(),
// 			sigma: proto.get_sigma().into(),
// 			batt_mem: proto.get_batt_mem().into()
// 		}
// 	}

// }

/*

use gsof_protocol::protocol::tables::{
    BattMemTable, ECEFTable, GPSTimeTable, HeadTable, SIGMATable,
};

*/

use prost_types;

impl Gsof {
    pub fn new() -> Self {
        ::std::default::Default::default()
    }

    pub fn get_dt_gen(&self) -> Option<prost_types::Timestamp> {
        match &self.dt_gen {
            Some(dt) => Some(dt.clone()),
            None => None,
        }
    }

    pub fn set_dt_gen(&mut self, val: prost_types::Timestamp) {
        self.dt_gen = Some(val);
    }

    pub fn get_dt_recv(&self) -> Option<prost_types::Timestamp> {
        match &self.dt_recv {
            Some(dt) => Some(dt.clone()),
            None => None,
        }
    }

    pub fn set_dt_recv(&mut self, val: prost_types::Timestamp) {
        self.dt_recv = Some(val);
    }

    pub fn get_checksum(&self) -> i32 {
        self.checksum
    }

    pub fn set_checksum(&mut self, val: i32) {
        self.checksum = val;
    }

    pub fn get_head(&self) -> Option<GsofHead> {
        self.head.clone()
    }

    pub fn set_head(&mut self, val: Option<HeadTable>) {
        match val {
            Some(head) => self.head = Some(GsofHead::from(head)),
            None => {}
        }
    }

    pub fn get_time(&self) -> Option<GsofTime> {
        self.time.clone()
    }

    pub fn set_time(&mut self, val: Option<GPSTimeTable>) {
        match val {
            Some(time) => self.time = Some(GsofTime::from(time)),
            None => {}
        }
    }

    pub fn get_ecef(&self) -> Option<GsofEcef> {
        self.ecef.clone()
    }

    pub fn set_ecef(&mut self, val: Option<ECEFTable>) {
        match val {
            Some(ecef) => self.ecef = Some(GsofEcef::from(ecef)),
            None => {}
        }
    }

    // sigma_table
    pub fn get_sigma(&self) -> Option<GsofSigma> {
        self.sigma.clone()
    }

    pub fn set_sigma(&mut self, val: Option<SIGMATable>) {
        match val {
            Some(sigma) => self.sigma = Some(GsofSigma::from(sigma)),
            None => {}
        }
    }

    // batt_mem
    pub fn get_optional_batt_mem(&self) -> Option<OptionalBattMem> {
        self.optional_batt_mem.clone()
    }

    pub fn set_optional_batt_mem(&mut self, val: Option<BattMemTable>) {
        match val {
            Some(bm) => {
                self.optional_batt_mem = Some(OptionalBattMem::BattMem(GsofBattMem::from(bm)))
            }
            None => {}
        }
    }
}

impl From<GsofProtocol> for Gsof {
    fn from(gsof: GsofProtocol) -> Self {
        let mut new_gsof = Gsof::new();
        new_gsof.set_dt_recv(dt_utc2ts(&gsof.dt_recv));
        new_gsof.set_dt_gen(dt_utc2ts(&gsof.dt_gen));
        new_gsof.set_checksum(gsof.checksum as i32);
        new_gsof.set_head(gsof.head);
        new_gsof.set_time(gsof.time);
        new_gsof.set_ecef(gsof.ecef);
        new_gsof.set_sigma(gsof.sigma);
        new_gsof.set_optional_batt_mem(gsof.batt_mem);
        new_gsof
    }
}
