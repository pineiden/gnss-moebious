use chrono::{DateTime, Utc};
use prost_types::Timestamp;
use std::time::SystemTime;

pub fn ts2dt_utc(ts: &Timestamp) -> Option<DateTime<Utc>> {
    let nts = ts.clone();
    match SystemTime::try_from(nts) {
        Ok(dt_time) => {
            let dt_time = DateTime::from(dt_time);
            Some(dt_time)
        }
        Err(_e) => None,
    }
}

pub fn dt_utc2ts(dt: &DateTime<Utc>) -> Timestamp {
    let seconds = dt.timestamp();
    let nanos = dt.timestamp_nanos() as i32;
    let timestamp = Timestamp { seconds, nanos };
    timestamp
}
