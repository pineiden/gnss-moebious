use crate::moebius_protocol::MeteoData;
use serde::{Serialize, Deserialize};
use chrono;
use chrono::prelude::*;
use crate::has_dt_gen::HasDtGen;
use crate::conversion::time::{dt_utc2ts, ts2dt_utc};

#[derive(Clone, PartialEq, Serialize, Deserialize)]
pub struct Meteo {
    pub dt_gen: DateTime<Utc>,
	pub temperature: f32,
	pub err_temperature: f32,
	pub humidity: f32,
	pub err_humidity: f32
}


impl From<MeteoData> for Meteo {
	fn from(data: MeteoData) -> Self {

		match data.get_dt_gen() {
			Some(date) => {
				let dt = ts2dt_utc(&date).unwrap();
				Meteo {
					dt_gen: dt,
					temperature: data.get_temperature(),
					err_temperature: data.get_err_temperature(),
					humidity: data.get_humidity(),
					err_humidity: data.get_err_humidity()
				}
			},
			None => {
				Meteo {
					dt_gen: Utc::now(),
					temperature: data.get_temperature(),
					err_temperature: data.get_err_temperature(),
					humidity: data.get_humidity(),
					err_humidity: data.get_err_humidity()
				}

			}

		}
	}
}


impl MeteoData {
    pub fn new() -> Self {
        ::std::default::Default::default()
    }

    pub fn get_dt_gen(&self) -> Option<prost_types::Timestamp> {
        match &self.dt_gen {
            Some(dt) => Some(dt.clone()),
            None => None,
        }
    }

    pub fn set_dt_gen(&mut self, val: prost_types::Timestamp) {
        self.dt_gen = Some(val);
    }

	pub fn get_temperature(&self) -> f32 {
		self.temperature
	}

	pub fn set_temperature(&mut self, val: f32) {
		self.temperature = val;
	}

	pub fn get_err_temperature(&self) -> f32 {
		self.err_temperature
	}

	pub fn set_err_temperature(&mut self, val: f32) {
		self.temperature = val;
	}

	pub fn get_humidity(&self) -> f32{
		self.humidity
	}

	pub fn set_humidity(&mut self, val: f32) {
		self.humidity = val;	
	}

	pub fn get_err_humidity(&self) -> f32 {
		self.err_humidity
	}

	pub fn set_err_humidity(&mut self, val: f32) {
		self.err_humidity = val;
	}

}


impl From<Meteo> for MeteoData {
	fn from(meteo: Meteo) -> Self {
		let mut new_meteo = MeteoData::new();
		new_meteo.set_dt_gen(dt_utc2ts(&meteo.dt_gen));
		new_meteo.set_temperature(meteo.temperature);
		new_meteo.set_err_temperature(meteo.temperature);
		new_meteo.set_humidity(meteo.temperature);
		new_meteo.set_err_humidity(meteo.temperature);
		new_meteo
	}
}


impl HasDtGen for Meteo {
	fn get_dt_gen(&self) -> DateTime<Utc> {
		self.dt_gen
	}
}
