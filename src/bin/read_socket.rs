#[macro_use]
extern crate clap;
extern crate gsof_protocol;
extern crate gnss_moebius;

use clap::{App, Arg};
use gnss_moebius::client::machine;
use tracing::info;
use tracing::{span, Instrument, Level};
use tracing_subscriber;

use gnss_moebius::validators::isnumeric;

#[doc = r#"
This command allows you to connect to a TCP Socket server (ip and port provided)
Can receive any set of data separated by a sign like /*end*/.
So you have to detect or define what is the separator to manage correctly the
stream of data.
With the command /--help/ you can access to the options and default values.
```
run_socket --help
```
"#]
#[tokio::main]
pub async fn main() {
    tracing_subscriber::fmt::init();
    let span_main = span!(Level::TRACE, "main");
    info!("Starting client for datagen server");
    // obtener parámetros de la línea de comandos

    let matches = App::new("Datagen Network Client CLI")
        .author(crate_authors!())
        .version(crate_version!())
        .about("This is the datagen server cli app")
        .arg(
            Arg::with_name("host")
                .short("h")
                .long("host")
                .takes_value(true)
                .value_name("HOST")
                .default_value("0.0.0.0")
                .case_insensitive(true)
                .help("Give a hostname, url or ip"),
        )
        .arg(
            Arg::with_name("port")
                .short("p")
                .long("port")
                .takes_value(true)
                .value_name("NAME")
                .default_value("8888")
                .validator(isnumeric)
                .help("Give a name for protocol"),
        )
        .arg(
            Arg::with_name("code")
                .short("c")
                .long("code")
                .takes_value(true)
                .value_name("CODE")
                .default_value("DATA")
                .help("Give a code name for source"),
        )
        .get_matches();


    if let Some(host) = matches.value_of("host") {
        info!("host {}", host);
    }
    let host = matches.value_of("host").unwrap().to_string();

    if let Some(port) = matches.value_of("port") {
        info!("port {}", port);
    }
    let port = matches
        .value_of("port")
        .unwrap()
        .to_string()
        .parse::<u16>()
        .unwrap();


    if let Some(code) = matches.value_of("code") {
        info!("code {}", code);
    }
    let code = matches
        .value_of("code")
        .unwrap()
        .parse::<String>()
        .unwrap();

    machine::client(&host, &port, &code)
        .instrument(span_main)
        .await;
}
