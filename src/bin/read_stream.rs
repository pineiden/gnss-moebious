use gsof_protocol::protocol::tables::read_gsof_file_protocol;
use gsof_protocol::protocol::gsof::{GsofData};
use tokio::{io};
use tracing::{span, Level, Instrument};
use tracing_subscriber;
use tracing::{info, error};


/*
Read a stream of bytes from stdout and convert to GsofData

run_socket --help
 */
#[tokio::main]
async fn main(){
    tracing_subscriber::fmt::init();
	let span_main = span!(Level::TRACE, "main");
    let stdin = io::stdin();
    let mut reader = io::BufReader::new(stdin);
    let tables = read_gsof_file_protocol().unwrap();
	info!("Tables GSOF are {:?}", tables);
    loop {
		let span_main = span_main.clone();
        match GsofData::read_from_stream(
            &mut reader, &tables)
			.instrument(span_main)
			.await {
				Ok(result) => {
        let latency = (result.latency() as f64) / 1000.0;
        info!("Latency {} secs", latency);

				}
				Err(_) => error!("Error al recibir Gsof")
				}

    }
}
