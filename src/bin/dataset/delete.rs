//extern crate clap;
#[macro_use]
extern crate clap;
use clap::{Arg, App};
use std::error::Error;

use chrono;
use chrono::prelude::*;

extern crate gnss_moebius;
use gnss_moebius::models::dataset::{Data};

use gnss_moebius::db::connection::establish_connection;
use std::process;

fn delete_dataset(sources:&Vec<String>) -> Result<(), Box<dyn Error + 'static>> {

    let connection = establish_connection()?;
	match Data::delete_all(&connection, sources){
		Ok(n)=> {
			println!("Deleted {} items on dataset, for {:?}", n, sources);
		},
		Err(err)=>println!("{}", err)
	}
    Ok(())
}

fn delete_dataset_between(
	sources:&Vec<String>, 
	t0:&DateTime<Utc>, 
	tf:&DateTime<Utc>) -> Result<(), Box<dyn Error + 'static>> {

    let connection = establish_connection()?;
	match Data::delete_between(&connection, sources, (*t0, *tf)){
		Ok(n)=> {
			println!(
				"Deleted {} items on dataset, sources {:?}, from {} to {}", 
				n, 
				sources, 
				t0, tf);
		},
		Err(err)=>println!("{}", err)
	}
    Ok(())
}


#[tokio::main]
pub async fn main()  {
    println!("Press {} to  finish (cut) the program", EOF);

    let matches = App::new("GNSS Moebious Updating Sources from table CLI")
        .author(crate_authors!())
        .version(crate_version!())
        .about("This is the moebious cli app")
        .arg(
            Arg::with_name("all")
                .short("a")
                .long("all")
                .takes_value(false)
                .help("If delete all or some"),
        )
        .arg(
            Arg::with_name("source")
                .short("s")
                .long("sources")
                .takes_value(true)
                .multiple(true)
                .value_name("Source list")
                .help("Give a source's codes"),
        )
        .arg(
            Arg::with_name("t0")
                .short("I")
                .long("start")
                .takes_value(true)
                .value_name("Code")
                .help("Give source's codes"),
        )
        .arg(
            Arg::with_name("tf")
                .short("F")
                .long("end")
                .takes_value(true)
                .value_name("Code")
                .help("Give source's codes"),
        )
        .get_matches();

	let all:bool = matches.is_present("all");
	
	if all {
		match establish_connection() {
			Ok(connection) => {
				match Data::delete(&connection){
					Ok(n) => println!("Borrados {} datos", n),
					Err(_) => println!("Error al borrar dataset")
				}
			},
			Err(e) => println!("{}",e)
		}
	} else {
		let exists_t0:bool = matches.is_present("t0");
		let exists_tf:bool = matches.is_present("tf");
		/*
		Considers the case:
		- both no exists
		- both exists
		- exist t0, then tf is now
		- exist tf, then t0 is the first value
		- exist both but tf>t0
		 */
		match matches.values_of("source"){
			Some(values) => {
				let sources:Vec<String> = values.map(
					|v| v.to_string().to_uppercase()
				).collect();
 
				if exists_t0 && exists_tf {
					let t0:DateTime<Utc> = matches.value_of("t0").unwrap().parse().unwrap();
					let tf:DateTime<Utc> = matches.value_of("tf").unwrap().parse().unwrap();

					match delete_dataset_between(&sources, &t0, &tf) {
						Err(err) => {
						   println!("Error running list_sources fn: {}", err);
						   process::exit(1);
						}
						_ => (),
					}
				} else {
					match delete_dataset(&sources) {
						Err(err) => {
						   println!("Error running list_sources fn: {}", err);
						   process::exit(1);
						}
						_ => (),
					}
				}
			},
			None => println!("No codes selected")
		}


	};

}

#[cfg(not(windows))]
const EOF: &'static str = "CTRL+D";

#[cfg(windows)]
const EOF: &'static str = "CTRL+Z";
