//extern crate clap;
#[macro_use]
extern crate clap;
use clap::{Arg, App};


extern crate gnss_moebius;
use gnss_moebius::models::dataset::{Data};

use gnss_moebius::db::connection::establish_connection;
use std::process;
use serde::ser::StdError;

fn list_dataset(sources:&Vec<String>) -> Result<(), Box<dyn StdError + 'static>> {

    let connection = establish_connection()?;
	match Data::list(&connection, sources){
		Ok(dataset)=> {
			for item in dataset.into_iter() {
				println!("{:?}", item);
			}
		},
		Err(err)=>return Err(err)
	}
    Ok(())
}

/*
Has to give ::

id -> UUID value
field -> field_name 
value -> JSON serialized/integer/datetime<UTC>
*/
#[tokio::main]
pub async fn main() {
    println!("Press {} to  finish (cut) the program", EOF);

    let matches = App::new("GNSS Moebious Updating Sources from table CLI")
        .author(crate_authors!())
        .version(crate_version!())
        .about("This is the moebious cli app")
        .arg(
            Arg::with_name("source")
                .short("s")
                .long("sources")
                .takes_value(true)
                .multiple(true)
                .value_name("Source list")
                .help("Give a source's codes"),
        )
        .get_matches();


	let sources:Vec<String> = matches.values_of("sources").unwrap().map(
		|v| v.to_string().to_uppercase()
	).collect();

    match list_dataset(&sources) {
        Err(err) => {
           println!("Error running list_sources fn: {}", err);
           process::exit(1);
        }
        _ => (),
    }
}

#[cfg(not(windows))]
const EOF: &'static str = "CTRL+D";

#[cfg(windows)]
const EOF: &'static str = "CTRL+Z";
