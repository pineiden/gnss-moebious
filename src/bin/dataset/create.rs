//extern crate clap;
#[macro_use]
extern crate clap;
use clap::{Arg, App, SubCommand};


extern crate gnss_moebius;
use gnss_moebius::models::dataset::{Data,  CsvData, CsvDataNoSource};
use gnss_moebius::models::sources::{Source};

use gnss_moebius::db::connection::establish_connection;
use std::io;
use std::collections::HashMap;
use std::error::Error;

fn create_dataset_from_stream(delimiter:&u8) -> Result<(),Box<dyn Error>> {
/*
Given a stream, that could be infinite, then can't accumulate values
	for ever.
*/
    let connection = establish_connection()?;
	let mut sources_index:HashMap<String,Source> = HashMap::new();
    let mut rdr = csv::ReaderBuilder::new()
		.delimiter(*delimiter)
        .quoting(true)
		.from_reader(io::stdin());
    for result in rdr.deserialize() {
		match result {
			Ok(record) => {
				let new_csv_data:CsvData = record;
				let ncd = new_csv_data.clone();
				let code:&str = new_csv_data.get_source();
				let this_code:&str = ncd.get_source();
				match sources_index.get(this_code) {
					Some(source) => {
						println!("{:?}", source);
						let new_data = new_csv_data.new_data(&source).unwrap();
						/* given source save data*/
						let data = Data::create_with_source(
							&connection, 
							&source,
							&new_data);
						println!("{:?}", data);    
					},
					None => {
						println!("None");
						let source = Source::read_by_code(
							&connection,
							&new_csv_data.source).unwrap();
						let new_data = ncd.new_data(&source).unwrap();
						/* given source save data*/
						let data = Data::create_with_source(
							&connection, 
							&source,
							&new_data);						
						// // add to cache-hashmap
						println!("{:?} <- {}", data, code);
						sources_index.insert(code.to_string(), source);
					}
					
				}
			},
			Err(e)=> {
				println!("Error on {}", e);
				return Err(Box::new(e))
			}
		}
		}
    Ok(())
}

use std::path::Path;
use std::ffi::OsStr;

fn create_dataset_from_files(
	files: &Vec<String>, 
	delimiter:&u8)-> Result<(),Box<dyn Error>> {
// 	/*
// 	parse to list of existing files and check csv props
// 	 */
	use rayon::prelude::*;

	files.par_iter().for_each(|file|{
		let path = Path::new(file);
		match path.exists(){
			true => {
				match path.extension() {
					Some(csv) if csv == OsStr::new("csv") => {
			   	 	   let connection = establish_connection().unwrap();
					   let mut sources_index:HashMap<String,Source> = HashMap::new();
						//open a reader csv
					   let mut rdr = csv::ReaderBuilder::new()
						 .delimiter(*delimiter)
						 .quoting(true)
						 .from_path(&file).unwrap();
					// read every file
					for result in rdr.deserialize() {
						match result {
							Ok(record) => {
								let new_csv_data:CsvData = record;
								let ncd = new_csv_data.clone();
								let code:&str = new_csv_data.get_source();
								let this_code:&str = ncd.get_source();
								match sources_index.get(this_code) {
									Some(source) => {
										println!("{:?}", source);
										let new_data = new_csv_data.new_data(&source).unwrap();
										/* given source save data*/
										let data = Data::create_with_source(
											&connection, 
											&source,
											&new_data);
										println!("{:?}", data);    
									},
									None => {
										println!("None");
										let source = Source::read_by_code(
											&connection,
											&new_csv_data.source).unwrap();
										let new_data = ncd.new_data(&source).unwrap();
										/* given source save data*/
										let data = Data::create_with_source(
											&connection, 
											&source,
											&new_data);						
										// // add to cache-hashmap
										println!("{:?} <- {}", data, code);
										sources_index.insert(code.to_string(), source);
									}

								}
							},
							Err(e)=> {
								println!("Error on {}", e);
							}
						} // end match
						}// end for reader

						()},
					Some(_x) => {()},
					None => {()}
				} /* end match extension */
			},
			false =>()//writeln!(stdout(), "No exists file {}", file).unwrap()
		} /* end match exists file*/
	});

    Ok(())
}

fn create_dataset_from_files_given_source(
	source:&Source, 
	files:&Vec<String>,
    delimiter:&u8)-> Result<(),Box<dyn Error>> {

	use rayon::prelude::*;

	files.par_iter().for_each(|file|{
		let path = Path::new(file);
		match path.exists(){
			true => {
				match path.extension() {
					Some(csv) if csv == OsStr::new("csv") => {
			   	 	   let connection = establish_connection().unwrap();
						//open a reader csv
					   let mut rdr = csv::ReaderBuilder::new()
						 .delimiter(*delimiter)
						 .quoting(true)
						 .from_path(&file).unwrap();
					// read every file
					for result in rdr.deserialize() {
						match result {
							Ok(record) => {
								let new_csv_data:CsvDataNoSource = record;
								println!("{:?}", source);
								let new_data = new_csv_data.new_data(source).unwrap();
								/* given source save data*/
								let data = Data::create_with_source(
									&connection, 
									&source,
									&new_data);
								println!("{:?}", data);    
							},
							Err(e)=> {
								println!("Error on {}", e);
							}
						} // end match
						}// end for reader

						()},
					Some(_x) => {()},
					None => {()}
				} /* end match extension */
			},
			false =>()//writeln!(stdout(), "No exists file {}", file).unwrap()
		} /* end match exists file*/
	});
    Ok(())
}

/*
This script creates from:

- command line with options.
- from stream output read at csv format 
- from a file read and accumulate

*/
#[tokio::main]
pub async fn main()  {
    println!("Press {} to  finish (cut) the program", EOF);

    let matches = App::new("GNSS Moebious Createing Data to DB from table CLI")
        .author(crate_authors!())
        .version(crate_version!())
        .about("This is the moebious cli app")
        .args(&[
            Arg::with_name("stream")
                .short("s")
                .long("stream")
                .help("Take from stdin"),
        Arg::with_name("file")
                .short("f")
                .long("file")
                .takes_value(true)
                .multiple(true)
                .value_name("FilePath")
                .help("Take from file(s)"),
         Arg::with_name("delimiter")
                .short("d")
                .long("delimiter")
                .takes_value(true)
                .value_name("Delimiter")
                .help("Delimiter of csv fields")]
        )
        .subcommand(
			SubCommand::with_name("given_source")
		    .about("given a source code, read values from file")
			.args(
			  &[Arg::with_name("source") // in case not stream and not file
				   .short("c")
				   .long("code")
				   .takes_value(true)
				   .value_name("Source code")
				   .help("Give a Source code where data is related"),
			   Arg::with_name("file") // in case not stream and not file
				   .short("f")
				   .long("file")
				   .takes_value(true)
				   .multiple(true)
				   .value_name("File(s) with data, without 'source' column")
				   .help("Give a path(s) list")]
			)
		)
        .get_matches();


    // match list_dataset(&sources) {
    //     Err(err) => {
    //        println!("Error running list_sources fn: {}", err);
    //        process::exit(1);
    //     }
    //     _ => (),
    // }

	let do_stream = matches.is_present("stream");
	let do_files = matches.is_present("file");

	let exists_delimiter = matches.is_present("delimiter");
	let mut delimiter:u8 = b';';
	if exists_delimiter {
		delimiter = matches.value_of("delimiter").unwrap().parse().unwrap();
	}

	/*
	takes from stream
	 */
	if do_stream {
		/* define streamr reader and give to read */
		match create_dataset_from_stream(&delimiter) {
			Ok(_) => println!("All data loaded ok"),
			Err(_) => println!("Error on reading stream")
		}
	}
	// /*
	// takes from files
	//  */
	if do_files {
		/* define file reader and give to read -> paralelize */
		match matches.values_of("file"){
			Some(values) => {
				let files:Vec<String> =	values.map(|f| f.to_string()).collect();
				match create_dataset_from_files(&files, &delimiter) {
					Ok(_) => {
						create_dataset_from_files(
							&files, 
							&delimiter).unwrap();
						println!("Loaded data from files {:?}", files)
					},
					Err(_) => println!("Error on reading stream")
				}
			},
			None => {println!("No list of files")}
		}
	}

	// }
	// /*
	// given code takes from files
	//  */
	if let Some(matches_given_source) = matches.subcommand_matches("given_source") {
		match matches_given_source.value_of("source") {
			Some(code) => {
				match matches_given_source.values_of("file") {
					Some(values) => {
						let files:Vec<String> =	values.map(|f| f.to_string()).collect();
						let connection = establish_connection().unwrap();
						let source = Source::read_by_code(
							&connection,
							&code.to_string()).unwrap();
						create_dataset_from_files_given_source(
							&source,
							&files, 
							&delimiter).unwrap();
					},
					None => println!("no file paths given")
				}
			},
			None => println!("No given code")
		}

	}


}

#[cfg(not(windows))]
const EOF: &'static str = "CTRL+D";

#[cfg(windows)]
const EOF: &'static str = "CTRL+Z";
