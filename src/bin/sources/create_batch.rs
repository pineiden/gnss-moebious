//extern crate clap;
extern crate gnss_moebius;
use gnss_moebius::models::sources::{Source};
use gnss_moebius::db::connection::establish_connection;
use std::error::Error;
use std::io;
use std::process;

fn read_csv() -> Result<(), Box<dyn Error>> {
    let connection = establish_connection()?;
    let mut rdr = csv::Reader::from_reader(io::stdin());
	let mut new_sources:Vec<Source> = vec![];
    for result in rdr.deserialize() {
		match result {
			Ok(data) => {
				let new_source:Source = data;
				new_sources.push(new_source);
			},
			Err(e)=> {
				println!("Error on {}", e);
				return Err(Box::new(e))
			}
		}
		}

    let sources = Source::create_batch(&connection, &new_sources);
    println!("{:?}", sources);
    
    Ok(())
}

#[tokio::main]
pub async fn main() {
    println!("Press {} to  finish (cut) the program", EOF);

    if let Err(err) = read_csv() {
       println!("Error running example: {}", err);
       process::exit(1);
    }
}

#[cfg(not(windows))]
const EOF: &'static str = "CTRL+D";

#[cfg(windows)]
const EOF: &'static str = "CTRL+Z";
