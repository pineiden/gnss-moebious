//extern crate clap;
extern crate gnss_moebius;
use gnss_moebius::models::sources::{Source};
use gnss_moebius::db::connection::establish_connection;
use std::process;
use serde::ser::StdError;

fn list_sources() -> Result<(), Box<dyn StdError + 'static>> {
    let connection = establish_connection()?;
	match Source::list(&connection){
		Ok(sources)=> {
			for item in sources.into_iter() {
				println!("{:?}", item);
			}
		},
		Err(err)=>return Err(err)
	}
    Ok(())
}

#[tokio::main]
pub async fn main() {
    println!("Press {} to  finish (cut) the program", EOF);

    match list_sources() {
        Err(err) => {
           println!("Error running list_sources fn: {}", err);
           process::exit(1);
        }
        _ => (),
    }
}

#[cfg(not(windows))]
const EOF: &'static str = "CTRL+D";

#[cfg(windows)]
const EOF: &'static str = "CTRL+Z";
