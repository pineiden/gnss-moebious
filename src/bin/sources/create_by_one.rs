//extern crate clap;
extern crate gnss_moebius;
use gnss_moebius::models::sources::{Source};
use gnss_moebius::db::connection::establish_connection;
use std::error::Error;
use std::io;
use std::process;

fn read_csv() -> Result<(), Box<dyn Error>> {
    let connection = establish_connection()?;
    let mut rdr = csv::Reader::from_reader(io::stdin());
    for result in rdr.deserialize() {
		match result {
			Ok(data) => {
				let new_source:Source = data;
				let source = Source::create(&connection, &new_source);
				println!("{:?}", source);    
			},
			Err(e)=> {
				println!("Error on {}", e);
				return Err(Box::new(e))
			}
		}
		}

    Ok(())
}

#[tokio::main]
pub async fn main() {
    println!("Press {} to  finish (cut) the program", EOF);

    if let Err(err) = read_csv() {
       println!("Error running example: {}", err);
       process::exit(1);
    }
}

#[cfg(not(windows))]
const EOF: &'static str = "CTRL+D";

#[cfg(windows)]
const EOF: &'static str = "CTRL+Z";
