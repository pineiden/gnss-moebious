/*
Delete only one or a set(array) or all
*/

//extern crate clap;
#[macro_use]
extern crate clap;
use clap::{Arg, App};

extern crate gnss_moebius;
use gnss_moebius::models::sources::{Source};
use gnss_moebius::db::connection::establish_connection;
use serde::ser::StdError;
//use tracing::{span, Instrument, Level};
//use tracing_subscriber;



#[tokio::main]
pub async fn main() -> Result<(), Box<dyn StdError + 'static>>{
    println!("Press {} to  finish (cut) the program", EOF);

    let connection = establish_connection()?;

    let matches = App::new("GNSS Moebious Deleting Sources from table CLI")
        .author(crate_authors!())
        .version(crate_version!())
        .about("This is the datagen server cli app")
        .arg(
            Arg::with_name("all")
                .short("a")
                .long("all")
                .takes_value(false)
                .help("If delete all or some"),
        )
        .arg(
            Arg::with_name("id")
                .short("i")
                .long("id")
                .takes_value(true)
                .multiple(true)
                .value_name("Id")
                .help("Give a source's ids"),
        )
        .arg(
            Arg::with_name("code")
                .short("c")
                .long("code")
                .multiple(true)
                .takes_value(true)
                .value_name("Code")
                .help("Give source's codes"),
        )
        .get_matches();

	let all:bool = matches.is_present("all");
	if all {
		println!("Deleting all {}", all);
		Source::delete_all(&connection).unwrap();
	} else {
		println!("Not all will be deleted");
		match matches.values_of("id") {
			Some(values)=>{ 
				let ids:Vec<i32> = values.map(|v| v.parse::<i32>().unwrap()).collect();
				println!("Deleting ids {:?}", ids);
				Source::delete_by_ids(&connection, &ids).unwrap();
			}, 
			None => println!("No ids selected")
		}

		match matches.values_of("code") {
			Some(values)=>{ 
				let codes:Vec<&str> = values.collect();
				Source::delete_by_codes(&connection, &codes).unwrap();
				println!("Deleting ids {:?}", codes);
			}, 
			None => println!("No codes selected")
		}

	}

	Ok(())

}

#[cfg(not(windows))]
const EOF: &'static str = "CTRL+D";

#[cfg(windows)]
const EOF: &'static str = "CTRL+Z";

