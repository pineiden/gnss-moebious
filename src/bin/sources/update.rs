/*
Delete only one or a set(array) or all
*/

//extern crate clap;
#[macro_use]
extern crate clap;
use clap::{Arg, App};

extern crate gnss_moebius;
use gnss_moebius::models::sources::{Source,VecValues};
use gnss_moebius::db::connection::establish_connection;
use serde::ser::StdError;
//use tracing_subscriber;

/// Update one or more field for one or more ids/codes

#[tokio::main]
pub async fn main() -> Result<(), Box<dyn StdError + 'static>>{
    println!("Press {} to  finish (cut) the program", EOF);

    let connection = establish_connection()?;
    let matches = App::new("GNSS Moebious Updating Sources from table CLI")
        .author(crate_authors!())
        .version(crate_version!())
        .about("This is the moebious cli app")
        .arg(
            Arg::with_name("id")
                .short("i")
                .long("id")
                .takes_value(true)
                .multiple(true)
                .value_name("Id")
                .help("Give a source's ids"),
        )
        .arg(
            Arg::with_name("code")
                .short("c")
                .long("code")
                .multiple(true)
                .takes_value(true)
                .value_name("Code")
                .help("Give source's codes"),
        )
        .arg(
            Arg::with_name("field")
                .short("f")
                .long("field")
                .takes_value(true)
                .value_name("Field")
                .help("Give a field name"),
        )
        .arg(
            Arg::with_name("value")
                .short("v")
                .long("value")
                .takes_value(true)
                .value_name("Value")
                .help("Give the new value for this field"),
        )
        .get_matches();


	let field = matches.value_of("field").unwrap();
	let value = matches.value_of("value").unwrap();
	 /*
	Update looking for ids
	 */
	 println!("Not all will be deleted");
	 match matches.values_of("id") {
		 Some(values)=>{ 
			 let ids:Vec<i32> = values.map(|v| v.parse::<i32>().unwrap()).collect();
			 println!("Updating ids {:?}", ids);
			 let val_ids = VecValues::Integers(ids);
			 let results = Source::update(&connection, &val_ids,
	 field, value);
			 println!("Updated: {:?}", results);
		 }, 
		 None =>println!("No ids selected")
	 }

	 /*
	Update looking for codes
	 */
	 match matches.values_of("code") {
		 Some(values)=>{ 
			 let codes:Vec<&str> = values.collect();
			 //Source::update_by_codes(&connection, &codes);
			 println!("Updating ids {:?}", codes);
			 let val_codes = VecValues::Strings(codes);
			 let results = Source::update(&connection, &val_codes, field, value);
			 println!("Updated: {:?}", results);
		 }, 
		 None =>println!("No codes selected")
	 }


	Ok(())
}


#[cfg(not(windows))]
const EOF: &'static str = "CTRL+D";

#[cfg(windows)]
const EOF: &'static str = "CTRL+Z";
