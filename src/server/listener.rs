/*
Creation of the listener: Server listen connections

TODO::

TLS autenticación.

*/
use tonic::{transport::Server};

use crate::moebius_protocol::moebius_server::MoebiusServer;
use crate::server::sender::{MyMoebius};
use std::net::{SocketAddr};
use tracing::{info};
use std::error::Error;

pub async fn run_server(
	addr: &String, 
) -> Result<(), Box<dyn Error>> {

    let moebius = MyMoebius::default();

    println!("Server listening on {}", addr);
	let socket_addr: SocketAddr = addr.parse().unwrap();
    match Server::builder()
		.concurrency_limit_per_connection(32)
        .add_service(MoebiusServer::new(moebius))
        .serve(socket_addr)
        .await {
			Ok(_) => {
				info!("Connected successfully"); 
				Ok(())
			},
			Err(err) => Err(err)?
		}
}
