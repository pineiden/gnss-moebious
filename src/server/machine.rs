/*
Create the principal fn to activate the RPC server
*/

use super::listener;
use tracing::{span, Level, Instrument};
use tracing::{info, error};

#[tracing::instrument(level = "info")]
pub async fn moebius_server(
	host: &String,
	port: &u16,
) {
	// create the uri. 
	let uri = format!("{}:{}", host, port);
	let span_serve = span!(Level::TRACE, "SERVE");

	info!("Starting datagen_server");
	info!("URI defined by {}", uri);
	// create the channels to communicate among the threads.
	// the important object is tx so the threads can subscribe to the channel
	tokio::spawn(async move {
		match listener::run_server(
			&uri)
			.instrument(span_serve)
			.await {
				Ok(client) => info!("Connected to {:?}", client),
				Err(e) => error!("Error con creating client connection
	{:?}", e)
			};
	});
}
