use tokio::sync::mpsc;
use crate::moebius_protocol::moebius_server::{Moebius};
use crate::moebius_protocol::{PingRequest, PingReply, InfoRequest, InfoReply,
			  StatusRequest, StatusReply, DataRequest, Gsof,MeteoData,
							  DataReply, data_reply};

use tonic::{Request, Response, Status};
use crate::db::connection::{establish_connection, PgPool};
use crate::models::dataset::{Data};
use crate::models::sources::{Source};
use crate::meteo::Meteo;
use lazy_static::lazy_static;
use async_once::AsyncOnce;
use chrono::{DateTime, Utc};
use crate::meteo::{Meteo as MeteoProtocol};
use gsof_protocol::protocol::gsof::{GsofData as GsofProtocol};
use crate::conversion::time::{dt_utc2ts, ts2dt_utc};
use gsof_protocol::protocol::gsof::GsofData;

lazy_static! {
    pub static ref PGPOOL: AsyncOnce<PgPool> = AsyncOnce::new(async  {
		 establish_connection().unwrap()
		});
}


#[derive(Debug, Default)]
pub struct MyMoebius {}


pub enum DataType {
	Meteo(MeteoProtocol), 
	Gsof(GsofProtocol)
}

// data_reply::Data
// enum DataRPC {
// 	Meteo(MeteoData),
// 	Gsof(Gsof)
// }

pub fn data2prost_data(value:DataType) ->
	Option<data_reply::Data>  {
		match value {
			DataType::Gsof(data) => {
				Some(data_reply::Data::Gsof(Gsof::from(data)))
			},
			DataType::Meteo(data) => {
				Some(data_reply::Data::Meteo(MeteoData::from(data)))
			},
		}

}



#[tonic::async_trait]
impl Moebius for MyMoebius {
	/*
	Implements those rpc functions

  rpc ping(PingRequest) returns (PingReply);

  rpc InfoDevice(InfoRequest) returns (InfoReply);

  rpc RecoverData(DataRequest) returns (stream DataReply);

  rpc DeviceStatus(StatusRequest) returns (stream StatusReply);

	The trait is described by the service. 
	Must implement the functions described as rpc inside the proto service

	 */

        async fn ping(
            &self,
            request: tonic::Request<PingRequest>,
        ) -> Result<tonic::Response<PingReply>, Status>  {
		Ok(Response::new(
			PingReply { msg: format!("Received {}", request.get_ref().mode)
			}
		))
	}

	/* send device info */

        type RecoverDataStream = tokio_stream::wrappers::ReceiverStream<Result<DataReply, Status>>;

        type DeviceStatusStream = tokio_stream::wrappers::ReceiverStream<Result<StatusReply, Status>>;


        async fn info_device(
            &self,
            _request: Request<InfoRequest>,
        ) -> Result<Response<InfoReply>, Status> {

		/*
		Create the fn to obtain the device data
		 */
		Ok(Response::new(
			InfoReply { 
				name: "MOEBIUS".to_string(), 
				memory: 100,
				energy: 100, 
				sources: vec!["VALN".to_string(), "METEO".to_string()]} 
		))
	}

	async fn recover_data(
		&self,
		request: Request<DataRequest>,
	) -> Result<Response<Self::RecoverDataStream>, 	tonic::Status> {

		// create an internal channel to send data
        let (tx, 
			 rx) = mpsc::channel(100);

		// get code source
		// convert timestamp to DateTime<Utc>
		// https://docs.rs/prost-types/0.2.0/prost_types/struct.Timestamp.html
		let source_code = request.get_ref().source.to_string();
		let start = match &request.get_ref().start {
			Some(start) =>
				ts2dt_utc(&start).unwrap(),
			None => Utc::now()
		};
		let end = match &request.get_ref().end {
			Some(end) =>
				ts2dt_utc(&end).unwrap(),
			None => Utc::now()
		};
		
		/*
		Here a query to obtain the data
		convert 
		dt_gen -> Option<Timestamp>

		 */
		let pool = PGPOOL.get().await;

		let source  = Source::read_by_code(&pool, &source_code).unwrap();
		match Data::list_between(&pool, &source_code, (start, end)) {
			Ok(dataset) => {
				dataset.into_iter().for_each(|data| {
					let dt = dt_utc2ts(&data.dt_gen);
					match source.data_type.as_ref() {
						"Meteo" => {
							let meteo:Meteo = serde_json::from_value(data.data).unwrap();
							let tx = tx.clone();
							tokio::spawn(async move{tx.send(Ok(DataReply {
								id: data.id.to_string(),
								dt_gen: Some(dt),
								data: data2prost_data(DataType::Meteo(meteo))
							})).await.unwrap();});
						},
						"Gsof" => {
							let gsof: GsofData = serde_json::from_value(data.data).unwrap();
							let tx = tx.clone();

							 tokio::spawn(async move{tx.send(Ok(DataReply {
								 id: data.id.to_string(),
								 dt_gen: Some(dt),
								 data: data2prost_data(DataType::Gsof(gsof))
							 })).await.unwrap();});
						},
						_ => {}
					} // end match
				}//  end for_each
			) // close for_each						
			},
			Err(_e) => return Err(tonic::Status::new(
				tonic::Code::Unavailable, "Can't read data from db".to_string())),
		}// end match query list			
	
        Ok(Response::new(
            tokio_stream::wrappers::ReceiverStream::new(rx)
        ))
            
	}


	async fn device_status(
		&self,
		request: tonic::Request<StatusRequest>,
	) -> Result<tonic::Response<Self::DeviceStatusStream>,
		tonic::Status> {

		// create an internal channel to send data
        let (tx, rx) = mpsc::channel(100);

		// her must be a loop that recover data every n seconds

		tokio::spawn(async move {
			let _pool = PGPOOL.get().await;

			// get code source
			let _mode = &request.get_ref().mode;
			let dt:DateTime<Utc> = Utc::now();
			let ts = dt_utc2ts(&dt);
			let meteo = Meteo {
					 dt_gen: dt,
					 temperature:25.0,
					 err_temperature:0.2,
					 humidity:90.0, 
					 err_humidity:0.3		
			};
			tx.send(Ok(StatusReply {
				 dt_gen: Some(ts),
				 uptime: 4234123213,
				 memory: 90,
				 energy: 80,
				 meteo: Some(MeteoData::from(meteo))
				 }
			)).await.unwrap();		
		
			
		}).await.unwrap();
		
        Ok(Response::new(
            tokio_stream::wrappers::ReceiverStream::new(rx)
        ))
            
	}

}



