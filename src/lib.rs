#[macro_use]
extern crate diesel;
extern crate dotenv;
// extern crate failure;
// extern crate failure_derive;

// is the file that define the database schema, automatized by diesel 
pub mod schema;
pub mod validators;
pub mod has_dt_gen;
pub mod client;
pub mod server;
pub mod conversion;
pub mod meteo;

pub mod moebius_protocol {
    tonic::include_proto!("moebius");
}

