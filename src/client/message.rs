use gsof_protocol::protocol::gsof::GsofData;
use tokio::sync::broadcast::Sender;
use tracing::{error, info};
use crate::db::connection::establish_connection;
use crate::models::dataset::{Data};
use crate::models::sources::{Source};
use std::error::Error;


/*

The Data create_with_source method needs an object that implements
traits HasDtGen and Serialize at least.
*/
#[doc = r#"
This async function is connected with the function that manage the TcpStream
an receive the data as bytes by a broadcast channel.
Using a simple criteria with the end_flag agroup all slices received and
deserialize to create the structured d
ata. 
"#]
#[tracing::instrument(level = "info")]
pub async fn receive_and_save_data(
    tx_source: Sender<GsofData>,
	source_code: &String
) -> Result<(), Box<dyn Error + Send + Sync>>{
    info!("Starting receive and save data for");
    let mut rx_source = tx_source.subscribe();
    match establish_connection(){
		Ok(pool_connection) => {
			let source = Source::read_by_code(&pool_connection, source_code).unwrap();
			loop {
				match rx_source.recv().await {
					Ok(result) => {
						info!("{}", result.json_dumps());
						let latency = (result.latency() as f64) / 1000.0;

						let data = Data::create_with_source(
							&pool_connection, 
							&source,
							&result);						

						info!("Latency {} secs", latency);                
						info!("Data received and saved {:?}", data);
					}
					Err(err) => {
						error!("Error on receive data, {}", err);
						return Err(Box::new(err))
					}
				}
			}
		},
		Err(err) => {
			error!("Error on connect db, {}", err);
			Err(err)
		}
	};
}
