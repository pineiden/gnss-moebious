use crate::client::message;
use crate::client::receiver;
use futures::future::{self};
use tokio::sync::broadcast::{self};
use tracing::{span, Instrument, Level};

#[doc = r#"
Create the instance for a socket client (TcpStream) connected with
the data receptor.

Uses data argument from cli to create the parameters.

Server Parameters:
- scheme: http/https
- host: localhost/ip
- port: valid port number

Receptor parameters:
- end-flag: &String

All the paramters must be simple string or numbers, so on the main
function (using clap mayb or args parsing) you can obtain or define
the values.
"#]
#[tracing::instrument(level = "info")]
pub async fn client(
    host: &String,
    port: &u16,
	code: &String
) {
    let (tx_source, _rx) = broadcast::channel(100);

    let addr =  format!("{}:{}", host, port);
    // couroutine to save the data
    // couroutine to be used as client to receive data and
    // send to span_data

    let recv_tx_source = tx_source.clone();

	let source_code = code.clone();

    let data_task = tokio::spawn(async move {
		let span_data = span!(Level::TRACE, "DATA");
        message::receive_and_save_data(recv_tx_source, &source_code)
            .instrument(span_data)
            .await.unwrap();
    });

    let client_tx_source = tx_source.clone();

    let client_task = tokio::spawn(async move {
		let span_client = span!(Level::TRACE, "CLIENT");
        receiver::run_client(&addr, client_tx_source)
            .instrument(span_client)
            .await.unwrap();
    });

	/* add meteo measurement task */

	/* add meteo measurement save data */


    let v = vec![data_task, client_task];
    let _outputs = future::try_join_all(v).await;
}
