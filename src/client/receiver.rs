use std::time::Duration;
use tokio::io::BufReader;
use tokio::net::TcpStream;
use tokio::sync::broadcast::Sender;
use tokio::time::sleep;
use tracing::{error, info};
use gsof_protocol::protocol::gsof::GsofData;
use gsof_protocol::protocol::tables::read_gsof_file_protocol;
use crate::has_dt_gen::HasDtGen;
use chrono;
use chrono::prelude::*;
use std::error::Error;

impl HasDtGen for GsofData {
	fn get_dt_gen(&self) -> DateTime<Utc> {
		self.dt_gen
	}
}


#[doc = r#"
This async function create and manage a client TcpStream connection.
In case of error or socket server fails, try to reconnect forever.
Just receive bytes and send by the broadcast channel. 
"#]
#[tracing::instrument(level = "info")]
pub async fn run_client(
    addr: &String,
    tx_source: Sender<GsofData>) 
-> Result<(), Box<dyn Error + Send + Sync>> {
    let tables = read_gsof_file_protocol().unwrap();
    
    loop {
        // connect part
        // this is a socket
        // let mut client_socket:TcpStream =
        match TcpStream::connect(&addr).await {
            Ok(mut client_socket) => {
                let local = client_socket.local_addr().unwrap();
                let remote = client_socket.peer_addr().unwrap();
                info!("Connected from {} to {}", local, remote);
                let tx_source = tx_source.clone();
                let (reader, mut  _writer) = client_socket.split();
                let mut bufreader = BufReader::new(reader);
                loop {
                    match GsofData::read_from_stream(
                        &mut bufreader, &tables).await {
                        Ok(result) => {
                            tx_source.send(result).unwrap();
                            // accumulate to vec lines
                            // convert when line is *end*
                        }
                        Err(_) => {
                            info!("Error at reading from socket");
                            break;
                        }
                    } 
                  }
                }
            Err(err) => {
                error!("Error on try connection, {}", err);
                sleep(Duration::from_secs(5)).await;
				return Err(Box::new(err))

            }            
            }
        };
}
